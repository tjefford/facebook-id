<?php

/**
 * Description: Example code for getting the Facebook ID from any user page or business page
 * Date: December 14, 2014
 * Author: Tyler Jefford
 * URL: http://tylerjefford.com/getting-a-users-facebook-id-using-php/
 */
 
 
 	//Set what page you want to scrape
 	$facebookPage = 'http://facebook.com/gizmodo';
 	
 	//Construct the url that we will CURL
 	$url = 'https://graph.facebook.com/?ids='.$facebookPage;
 	
 	//Initiate the CURL call
 	$ch = curl_init($url);
 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
 	$response = json_decode(curl_exec($ch), true);
 	
 	//Use the facebookPage variable as a key to return data
 	
 	//ID
 	echo 'ID: '. $response[$facebookPage]['id'];
 	
 	//Below are business page only paramaters
 	
 	//Cover Photo URL
 	echo 'Cover Photo: '. $response[$facebookPage]['cover']['source'];
 	
 	//Founded
 	echo 'Founded: '. $response[$facebookPage]['founded'];
 	
 	//Company Overview
 	echo 'Company Overview: '. $response[$facebookPage]['company_overview'];
 	
 	//Talking About Page Count
 	echo 'Talking About: '. $response[$facebookPage]['talking_about_count'];
 
 	//Website
 	echo 'Website'. $response[$facebookPage]['website'];
 	
 	/* Of corse there are more items you can use, and it varies page to page, depending on how much data is available.
 	 * You can see all the data you have availble by doing a var_dump() of the response.
 	 *
 	 * var_dump($response);
 	 */
 	
 ?>